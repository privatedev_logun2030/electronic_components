'use strict';

var _typeof = typeof Symbol === "function" && typeof Symbol.iterator === "symbol" ? function (obj) { return typeof obj; } : function (obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol ? "symbol" : typeof obj; };

function isTouchDevice() {
	return !!('ontouchstart' in window) || !!('onmsgesturechange' in window);
}
/* global isTouchDevice */
window.addEventListener('load', function () {
	// Выпадающее меню
	var menuBtn = $('#header-section__menu-btn'),
	    mMenu = $('#header-section__sub-menu'),
	    menuLi = mMenu.find('.header-section__sub-menu-li'),
	    menuWrap = $(document.querySelector('.header-section__top'));
	var int;

	// Отмена кликов по некоторым ссылкам
	menuLi.find('a.not-link').click(function (e) {
		e.preventDefault();
	});

	// плавающее верхнее меню
	var h_hght = $('#body').hasClass('home') ? 776 : 204;
	var h_mrg = 0;
	var hst = $(document.querySelector('.header-section__top'));
	var bgw = $(document.querySelector('.header-section__bg-wrapper'));

	var top = $(this).scrollTop();

	var st = true;

	if (top > h_hght) {
		hst.addClass('pos-f ctr');
		bgw.addClass('pos-f');
		if ($('#body').hasClass('home')) {
			menuBtn.removeClass('open');
		}
	} else {
		hst.removeClass('pos-f ctr');
		bgw.removeClass('pos-f');
		if ($('#body').hasClass('home')) {
			menuBtn.addClass('open');
		}
	}
	$(window).scroll(function () {
		var top = $(this).scrollTop();

		if (top > h_hght) {
			hst.addClass('pos-f ctr');
			bgw.addClass('pos-f');
			$('#body').css({ marginTop: 70 });
			if (st && $('#body').hasClass('home')) {
				menuBtn.removeClass('open');
			}
			st = false;
		} else {
			hst.removeClass('pos-f ctr');
			bgw.removeClass('pos-f');
			$('#body').css({ marginTop: 0 });
			if ($('#body').hasClass('home')) {
				menuBtn.addClass('open');
			}
			st = true;
		}
	});

	menuLi.hover(function (e) {
		var $this = $(this);
		menuLi.removeClass('active');
		clearInterval(int);
		if ($this.find('.header-section__sub-sub-menu').length > 0) {
			$this.addClass('active');
		}
	}, function (e) {
		int = setTimeout(function () {
			menuLi.removeClass('active');
		}, 1000);
	});

	mMenu.mouseleave(function (event) {
		int = setTimeout(function () {
			if (menuWrap.hasClass('pos-f') || !$('#body').hasClass('home')) {
				menuBtn.removeClass('open');
			}
		}, 1000);
	});

	// Если главная
	if ($('#body').hasClass('home')) {
		menuBtn.find('.header-section__menu-p').click(function () {
			if (hst.hasClass('pos-f')) {
				menuBtn.toggleClass('open');
			}
		});
		// Первый слайдер
		$(document.querySelector('.header-section__slider')).slick({
			dots: true,
			infinite: true,
			speed: 500,
			fade: true,
			cssEase: 'linear',
			prevArrow: null,
			nextArrow: null,
			autoplay: true
		});

		// Ротатор с товарами
		$('.b-goods').each(function () {
			var $wrap = $(this),
			    $tabsLink = $wrap.find('.b-goods-tabs a'),
			    $arrow = $wrap.find('.b-goods-arrow'),
			    $gitems = $wrap.find('.b-goods-gitem'),
			    $items = $wrap.find('.b-goods-item'),
			    $control = $wrap.find('.b-goods-control'),
			    init = function init() {
				var activeIndex = $tabsLink.filter('.m-active').length ? $tabsLink.filter('.m-active').index() : 0;
				$tabsLink.removeClass('m-active').eq(activeIndex).addClass('m-active');
				$gitems.hide().eq(activeIndex).show();

				$gitems.each(function () {
					var maxLeft = $(this).find('.b-goods-item').length * 200 - 1200;
					if (maxLeft <= 0) {
						maxLeft = 0;
					}
					$(this).data('left', 0).data('maxleft', maxLeft);
				});

				if (Number($gitems.eq(activeIndex).data('maxleft')) === 0) {
					$control.hide();
				} else {
					$control.show();
				}

				$tabsLink.on('click', changeGitems);

				$arrow.on('click', moveGoods);
			},
			    changeGitems = function changeGitems(e) {
				e.preventDefault();
				if ($(this).hasClass('m-active')) {
					return;
				}

				var $active = $gitems.eq($tabsLink.filter('.m-active').index()),
				    $next = $gitems.eq($(this).index());

				$tabsLink.removeClass('m-active');
				$(this).addClass('m-active');

				if (Number($next.data('maxleft')) === 0) {
					$control.hide();
				} else {
					$control.show();
				}

				$active.fadeOut('fast', function () {
					$next.fadeIn('fast');
				});
			},
			    moveGoods = function moveGoods(e) {
				e.preventDefault();
				var vector = $(this).hasClass('m-right'),
				    $active = $gitems.eq($tabsLink.filter('.m-active').index()),
				    left = Number($active.data('left')),
				    maxLeft = Number($active.data('maxleft'));

				left = vector ? left + 1200 : left - 1200;
				console.log(left);
				console.log(maxLeft);
				if (vector) {
					if (left === maxLeft + 1200) {
						left = 0;
					}
					if (left > maxLeft) {
						left = maxLeft;
					}
				} else {
					if (left === 0 - 1200) {
						left = maxLeft;
					}
					if (left < 0) {
						left = 0;
					}
				}

				$active.data('left', left);

				$active.stop().animate({ left: -left }, 'slow');
			};

			init();
		});

		// Если остальные
	} else {
		menuBtn.find('.header-section__menu-p').click(function () {
			menuBtn.toggleClass('open');
		});
	}

	// Блок товаров водопадом
	$('#masonry-goods-section').masonry({
		itemSelector: '.masonry-section__item'
	});

	// Карусель новостей
	$('.b-carousel').each(function () {
		var $carousel = $(this),
		    $drag = $carousel.find('.b-carousel-items'),
		    $items = $carousel.find('.b-carousel-item'),
		    $pages = null,
		    $pagesLi = null,
		    itemInPage = -1,
		    interval = null,
		    maxLeft = -1,
		    width = -1,
		    itemWidth = -1,
		    current = 0,
		    maxCurrent = 0,
		    delay = 300000000,
		    speed = 'slow',
		    active = false,
		    init = function init() {
			if (!$carousel.data('scount')) {
				return;
			}
			itemInPage = Number($carousel.data('scount'));

			if ($carousel.data('pages')) {
				$pages = $('#' + $carousel.data('pages'));
				var html = '<li class="m-active"></li>',
				    l = Math.ceil($items.length / itemInPage);
				while (--l) {
					html += '<li></li>';
				}
				$pagesLi = $pages.html(html).find('li');

				$pagesLi.on('click', clickPages);
			}

			if ($items.length <= itemInPage) {
				if ($pages !== null) {
					$pages.hide();
				}
				return;
			}

			width = $carousel.outerWidth();
			itemWidth = $items.eq(0).outerWidth(true);
			maxLeft = itemWidth * $items.length - width;
			maxCurrent = Math.ceil($items.length / itemInPage);

			$carousel.on('mouseenter', autoStop).on('mouseleave', autoStart);
			autoStart();
		},
		    move = function move(index) {
			if (active) {
				return;
			}
			active = true;

			if ((typeof index === 'undefined' ? 'undefined' : _typeof(index)).toLowerCase() === 'boolean') {
				index = index ? current + 1 : current - 1;
			}

			if (index < 0) {
				index = maxCurrent;
			}
			if (index > maxCurrent) {
				index = 0;
			}

			current = index;

			if ($pages !== null) {
				$pagesLi.removeClass('m-active').eq(current).addClass('m-active');
			}

			var left = width * index;
			if (left > maxLeft) {
				left = maxLeft;
			}
			$drag.stop().animate({ marginLeft: -left }, speed, function () {
				active = false;
			});
		},
		    clickPages = function clickPages(e) {
			e.preventDefault();
			move($(this).index());
		},
		    auto = function auto() {
			move(true);
		},
		    autoStart = function autoStart() {
			clearInterval(interval);
			interval = setInterval(auto, delay);
		},
		    autoStop = function autoStop() {
			clearInterval(interval);
		};

		init();
	});

	// Перестройка блоков с товарами на внутренних страницах
	// строим ряды при сокращенном виде товара
	var t = 0;
	var filterPr = $('#filter-product');
	var bgr = filterPr.find('.b-goods-row');
	var l = bgr.length;
	bgr.each(function (i, el) {
		var $el = $(el);
		$el.css({
			top: t,
			zIndex: l - i
		});
		t += 375;
	});
	filterPr.css({ height: t + 100 });

	// Переключение видов отображения товаров
	var goodsCell = $(document.querySelector('.content-goods-cell'));
	var lin = $('#linear-view');
	var tbl = $('#table-view');
	var sliceGoodsName = function sliceGoodsName() {
		$('.goods-section .b-goods-item-link > em').each(function (i, el) {
			var $el = $(el);
			var t = $el.text().slice(0, 46) + ' ...';
			$el.attr('title', $el.text());
			if ($el.text().length > 50 && goodsCell.attr('id') !== 'linear-view-goods') {
				$el.text(t);
			}
		});
	};
	sliceGoodsName();

	lin.click(function () {
		if (goodsCell.attr('id') === 'linear-view-goods') {
			return false;
		}
		goodsCell.attr('id', 'linear-view-goods');
		$('.goods-section .b-goods-item-link > em').each(function (i, el) {
			var $el = $(el);
			$el.text($el.attr('title'));
			$.cookie('linear_view', 'true');
		});
		lin.addClass('view-active');
		tbl.removeClass('view-active');
	});
	tbl.click(function () {
		if (goodsCell.attr('id') === '') {
			return false;
		}
		goodsCell.attr('id', '');
		sliceGoodsName();
		lin.removeClass('view-active');
		tbl.addClass('view-active');
	});
});